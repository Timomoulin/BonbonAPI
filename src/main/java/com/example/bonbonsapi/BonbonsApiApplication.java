package com.example.bonbonsapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BonbonsApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(BonbonsApiApplication.class, args);
    }

}
