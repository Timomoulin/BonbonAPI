package com.example.bonbonsapi.models.models.repositories;


import com.example.bonbonsapi.models.models.entities.Commande;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommandeRepository extends JpaRepository<Commande, Long> {
    long countByUtilisateur_Email(String email);

}