package com.example.bonbonsapi.models.models.repositories;


import com.example.bonbonsapi.models.models.entities.Categorie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategorieRepository extends JpaRepository<Categorie, Long> {




}