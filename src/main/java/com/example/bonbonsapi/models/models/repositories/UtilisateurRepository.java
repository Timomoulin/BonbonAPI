package com.example.bonbonsapi.models.models.repositories;


import com.example.bonbonsapi.models.models.entities.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {

    public Utilisateur findByEmail(String email);


}