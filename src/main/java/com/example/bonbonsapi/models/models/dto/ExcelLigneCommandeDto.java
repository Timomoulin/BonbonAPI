package com.example.bonbonsapi.models.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ExcelLigneCommandeDto {
    int quantite;
    long idInventaire;
}
