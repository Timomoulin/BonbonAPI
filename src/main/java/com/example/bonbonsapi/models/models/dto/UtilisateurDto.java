package com.example.bonbonsapi.models.models.dto;

import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UtilisateurDto implements Serializable {
    private  long id;
    private  String nom;
    private  String prenom;
    private  String email;
    private  String mdp;
    private  long roleId;




}
