package com.example.bonbonsapi.models.models.repositories;


import com.example.bonbonsapi.models.models.entities.Categorie;
import com.example.bonbonsapi.models.models.entities.Inventaire;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InventaireRepository extends JpaRepository<Inventaire, Long> {

    List<Inventaire>  findInventaireByProduit_Categorie(Categorie categorie);
}