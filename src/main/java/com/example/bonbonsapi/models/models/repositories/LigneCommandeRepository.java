package com.example.bonbonsapi.models.models.repositories;


import com.example.bonbonsapi.models.models.entities.LigneCommande;
import com.example.bonbonsapi.models.models.entities.LigneId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LigneCommandeRepository extends JpaRepository<LigneCommande, LigneId> {
}