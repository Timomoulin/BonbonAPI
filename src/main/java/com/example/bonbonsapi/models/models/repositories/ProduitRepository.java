package com.example.bonbonsapi.models.models.repositories;


import com.example.bonbonsapi.models.models.entities.Produit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProduitRepository extends JpaRepository<Produit, Long> {


    @Query(value = "select * from produit order by categorie_id,nom",nativeQuery = true)
    public List<Produit> foobar();

    List<Produit> findByOrderByCategorie_NomAscNomAsc();


}