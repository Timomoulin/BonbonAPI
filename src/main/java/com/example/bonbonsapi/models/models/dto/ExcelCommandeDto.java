package com.example.bonbonsapi.models.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ExcelCommandeDto {
    Date date;
    String statut;
    String initialesClient;
    Set<ExcelLigneCommandeDto> excelLigneCommandeDtoSet=new HashSet<ExcelLigneCommandeDto>();

    public ExcelCommandeDto(Date date, String statut, String initialesClient) {
        this.date = date;
        this.statut = statut;
        this.initialesClient = initialesClient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExcelCommandeDto that = (ExcelCommandeDto) o;

        if (!date.equals(that.date)) return false;
        if (!statut.equals(that.statut)) return false;
        return initialesClient.equals(that.initialesClient);
    }

    @Override
    public int hashCode() {
        int result = date.hashCode();
        result = 31 * result + statut.hashCode();
        result = 31 * result + initialesClient.hashCode();
        return result;
    }
}
