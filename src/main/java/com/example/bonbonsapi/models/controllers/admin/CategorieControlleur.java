package com.example.bonbonsapi.models.controllers.admin;

import com.example.bonbonsapi.models.models.dto.CategorieDto;
import com.example.bonbonsapi.models.models.entities.Categorie;
import com.example.bonbonsapi.services.CategorieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@CrossOrigin(origins = "*")
@Controller
@RequestMapping("/bonbons")
public class CategorieControlleur{

    @Autowired
    CategorieService categorieService;

    @GetMapping({"/admin/categories","/categories"})
    public ResponseEntity<List<Categorie>> index(){
        return new ResponseEntity<>(categorieService.index(), HttpStatus.ACCEPTED);
    }

    @GetMapping({"/admin/categories/{id}","/categories/{id}"})
    public ResponseEntity<Categorie> show(@PathVariable(name = "id") long id){
        return new ResponseEntity<>(categorieService.show(id),HttpStatus.FOUND);
    }

    @PostMapping("/admin/categories")
    @PreAuthorize("hasAuthority('admin')")
    public ResponseEntity<Categorie> store(@RequestBody CategorieDto categorieDto){
        Categorie cat= this.categorieService.store(categorieDto);
        return new ResponseEntity<>(cat,HttpStatus.CREATED);
    }

    @PutMapping("/admin/categories/{id}")
    @PreAuthorize("hasAuthority('admin')")
    public ResponseEntity<Categorie> update(@PathVariable(name = "id") long id ,@RequestBody CategorieDto categorieDto){
        Categorie cat= this.categorieService.update(id,categorieDto);
        return  new ResponseEntity<>(cat,HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/admin/categories/{id}")
    @PreAuthorize("hasAuthority('admin')")
    public ResponseEntity<Boolean> delete(@PathVariable("id") long id){
        return new ResponseEntity<>(this.categorieService.destroy(id),HttpStatus.ACCEPTED);
    }
}
