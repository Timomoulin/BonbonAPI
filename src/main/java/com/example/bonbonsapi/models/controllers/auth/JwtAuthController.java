package com.example.bonbonsapi.models.controllers.auth;


import com.example.bonbonsapi.config.JwtTokenUtil;
import com.example.bonbonsapi.models.models.dto.JwtRequest;
import com.example.bonbonsapi.models.models.dto.JwtResponse;
import com.example.bonbonsapi.models.models.dto.UtilisateurDto;
import com.example.bonbonsapi.models.models.entities.Utilisateur;
import com.example.bonbonsapi.models.models.repositories.UtilisateurRepository;
import com.example.bonbonsapi.services.JPAUserDetailsService;
import com.example.bonbonsapi.services.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class JwtAuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    UtilisateurService utilisateurService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JPAUserDetailsService userDetailsService;

    @PostMapping(value = "/inscription",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> register(@RequestBody UtilisateurDto utilisateur){
        utilisateur.setRoleId(2);
       Utilisateur newUtilisateur= utilisateurService.store(utilisateur);
       return  ResponseEntity.ok("ok");
    }

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {

        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

         UserDetails userDetails = userDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());

        final String token = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new JwtResponse(token));
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
