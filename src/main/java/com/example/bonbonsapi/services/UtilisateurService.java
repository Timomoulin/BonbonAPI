package com.example.bonbonsapi.services;

import com.example.bonbonsapi.models.models.entities.Utilisateur;
import com.example.bonbonsapi.models.models.dto.UtilisateurDto;
import com.example.bonbonsapi.models.models.repositories.RoleRepository;
import com.example.bonbonsapi.models.models.repositories.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;

@org.springframework.stereotype.Service
public class UtilisateurService {

    @Autowired
    private UtilisateurRepository repository;

    @Autowired
    private RoleRepository roleRepository;

    /**
     * Recupere une liste de Utilisateur
     *
     * @return List<Utilisateur>
     */
    public List<Utilisateur> index() {
        return repository.findAll();
    }

    /**
     * Recupere un Utilisateur pas son id
     *
     * @param id
     * @return un objet utilisateur
     */
    public Utilisateur show(long id) {
        return this.repository.findById(id).get();
    }


    public Utilisateur store(UtilisateurDto dto) {
        Utilisateur newUtilisateur = this.convertDtotoModel(dto);
       return repository.save(newUtilisateur);
    }

    /**
     * Met a jour un Utilisateur
     *
     * @param id
     * @param dto
     */
    public void update(Long id, UtilisateurDto dto) {
        if (null == id) {
            Utilisateur newUtilisateur = this.convertDtotoModel(dto);
            repository.save(newUtilisateur);
        }

    }

    /**
     * Supprime un Utilisateur
     *
     * @param id
     */
    public void destroy(Long id) {
        repository.deleteById(id);
    }

    /**
     * Convertion d'un DTO en Utilisateur
     *
     * @param dto
     * @return un objet Utilisateur
     */
    public Utilisateur convertDtotoModel(UtilisateurDto dto) {
        Utilisateur utilisateur=repository.findById(dto.getId()).orElse(new Utilisateur());
        utilisateur.setEmail(dto.getEmail());
        utilisateur.setMdp(new BCryptPasswordEncoder().encode(dto.getMdp()));
        utilisateur.setNom(dto.getNom());
        utilisateur.setPrenom(dto.getPrenom());
        utilisateur.setRole(roleRepository.findById(dto.getRoleId()).orElse(roleRepository.findById(2L).get()));
        return utilisateur;
    }


    public UtilisateurDto convertModeltoDTO(Utilisateur model) {
    UtilisateurDto dto= new UtilisateurDto(model.getId(), model.getNom(), model.getPrenom(), model.getEmail(), model.getMdp(), model.getRole().getId());
    return dto;
    }


}
