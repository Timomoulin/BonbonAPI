package com.example.bonbonsapi.services;

import com.example.bonbonsapi.models.models.dto.CategorieDto;
import com.example.bonbonsapi.models.models.entities.Categorie;
import com.example.bonbonsapi.models.models.repositories.CategorieRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@org.springframework.stereotype.Service
public class CategorieService {

    @Autowired
    private CategorieRepository repository;


    public List<Categorie> index() {
        return repository.findAll();
    }


    public Categorie show(long id) {
        return repository.findById(id).get();
    }


    public Categorie store(CategorieDto dto) {
        Categorie newCategorie = this.convertDTOtoModel(dto);
       return repository.save(newCategorie);
    }


    public Categorie update(Long id, CategorieDto dto) {
        if (null != id) {
            Categorie newCategorieService = this.convertDTOtoModel(dto);
            return repository.save(newCategorieService);
        }
        return null;

    }


    public Boolean destroy(Long id) {
        repository.deleteById(id);
        return true;
    }


    public Categorie convertDTOtoModel(CategorieDto dto) {
        Categorie cat= this.repository.findById(dto.getId()).orElse(new Categorie());
        cat.setNom(dto.getNom());
        return cat;
    }


    public CategorieDto convertModeltoDTO(Categorie model) {
        CategorieDto dto= new CategorieDto(model.getId() ,model.getNom());
        return  dto;
    }


}
