--Categories--
insert into CATEGORIE (NOM) values  ('chocolats'),('réglisses'),('artisanaux') ;

--Produits--
insert  into PRODUIT (IMAGE, NOM, CATEGORIE_ID) VALUES ( 'bonbon1.jpg','zanzigliss',2 ),('bonbon2.jpg','caramel',3), ('bonbon3.jpg','nougat',3);
--Inventaire--
insert into INVENTAIRE (PORTION,PRIX,QUANTITE,PRODUIT_ID) values ( '100g',10,127,1 ),( '50g',6,32,1 ),( '1Kg',25,8,1 );
insert into INVENTAIRE (PORTION,PRIX,QUANTITE,PRODUIT_ID) values ( '100g',10,64,2 ),( '50g',6,16,2 );
insert into INVENTAIRE (PORTION,PRIX,QUANTITE,PRODUIT_ID) values ( '100g',10,100,3 ),( '50g',6,52,3 );

--Roles--
insert into ROLE (NOM) values ( 'admin' );
insert into ROLE (NOM) values ( 'client' );

--Utilisateur--
insert into UTILISATEUR (EMAIL, MDP, NOM, PRENOM, ROLE_ID) values ( 'timomoulin@msn.com','$2a$10$FUl/QbFWX/wHYsKpuIpiy.1.Z0i2C3ce/QaIJke8vmMRzYe.dlWoO','moulin','timothée',1 );
insert into UTILISATEUR (EMAIL, MDP, NOM, PRENOM, ROLE_ID) values ( 'eric@gmail.com','{bcrypt}$2a$10$V0fighZkwE18DzpkhVUh7urFTo14o1gY9fx9727sQVXybfpq.bbeC','cartman','eric',2 );
insert into UTILISATEUR (EMAIL, MDP, NOM, PRENOM, ROLE_ID) values ( 'stan@gmail.com','{bcrypt}$2a$10$V0fighZkwE18DzpkhVUh7urFTo14o1gY9fx9727sQVXybfpq.bbeC','marsh','stan',2 );
insert into UTILISATEUR (EMAIL, MDP, NOM, PRENOM, ROLE_ID) values ( 'eric@gmail.com','{bcrypt}$2a$10$V0fighZkwE18DzpkhVUh7urFTo14o1gY9fx9727sQVXybfpq.bbeC','nroflovski','kyle',2 );